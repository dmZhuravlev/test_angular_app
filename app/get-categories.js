var mongoose = require('mongoose');
var Category = mongoose.model('Category');

module.exports = function (req, res) {

    Category.find(function (err, categories) {

        if (err) {
            res.status(400).send(err);
        } else {
            res.status(200).send({
                data: categories
            });
        }
    });
};
