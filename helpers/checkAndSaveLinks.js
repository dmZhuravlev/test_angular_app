// example
//var ItemSchema = new Schema({
//    name: { type: String, required: true, trim: true }
//},
//{
//    timestamps: true
//});
module.exports = function (data, res, id) {
    var cheerio = require('cheerio');
    var $;
    var description = '';
    var url = data.request.url;
    var mongoose = require('mongoose');
    var Category = mongoose.model('Category');

    var saveLink = function (url, title, description, id, isImg) {
        Category.findOneAndUpdate({'_id': id},
        {
            $push: {
                'links': {
                    isImg: isImg || false,
                    url: url || '',
                    title: title || '',
                    category: id,
                    comments: [],
                    description: description || ''
                }
            }
        }, function(err) {
            if (err) {
                return res.status(400).send('save link failed ' + err);
            }
            return res.status(200).send('save link success');

        });
    };

    if (/image/g.test(data.headers['content-type'])) {
        return saveLink(url, '', '', id, true);
    }
    if (!data.text) {
        return res.status(400).send('url is not correct');
    }

    $ = cheerio.load(data.text);

    $('meta').each(function (i, element) {
        if ($(element).attr('name') === 'description') {
            description = $(element).attr('content');
        }
    });

    saveLink(url, $('title').text(), description, id);
};