import template from './modalContent.html';

export default class MainContentController {
    constructor($http, $uibModal, CategoriesService) {
        'ngInject';
        this.$http = $http;
        this.$uibModal = $uibModal;
        this.getCategoriesService = CategoriesService;
    }

    $onInit() {
        this.getData();
    }

    getData () {
        this.getCategoriesService.getCategories().then((data) => {
            this.getCategories(data);
        });
        //this.$http.get('/api/getCategories').then((data) => {
        //    this.getCategories(data);
        //});
    }

    getCategories (data) {
        this.categories = data.data;
        this.activeCategory = this.categories[0];
        //this.activeCategoryLinks = this.activeCategory.links;
    }

    open () {
        this.$uibModal.open({
            template,
            controller: ModalController,
            bindToController: true,
            controllerAs: 'modalCtrl',
            resolve: {
                categories: () => this.categories,
                activeCategory: () => this.activeCategory
            }

        });
    }

}

class ModalController {
    constructor($uibModal, categories, activeCategory, $http, $scope) {
        'ngInject';
        this.$uibModal = $uibModal;
        this.categories = categories;
        this.activeCategory = activeCategory;
        this.$http = $http;
        this.scope = $scope;
        this.siteInfo = {
            title: '',
            description: '',
            url: ''
        };
        this.previewContent = false;
        this.imgSrc = '';
    }
    checkResponseData (data) {
        return (data.img || data.title);
    }

    save () {
        this.$http.post('/api/saveLinks', {
            'id': this.activeCategory._id,
            'url': this.urlInput
        })
        .success(data => {
            console.log(data);
        })
        .error(err => {
            console.log('error', err);
        });
    }
    close () {
        this.$close();
    }
    setDataInfo (data) {
        this.previewContent = true;
        this.imgSrc = data.img;
        this.siteInfo.url = data.url;
        this.siteInfo.title = data.title;
        this.siteInfo.description = data.description;
    }
    removeDataInfo () {
        this.previewContent = false;
        this.imgSrc = '';
        this.siteInfo.url = '';
        this.siteInfo.title = '';
        this.siteInfo.description = '';
    }
    checkUrl () {
        this.removeDataInfo();
        this.$http.post('/api/checkLinks', {
            'url': this.urlInput
        })
        .success(data => {
            if (this.checkResponseData(data)) {
                this.setDataInfo(data);
            }
        })
        .error(err => {
            this.removeDataInfo();
            console.log('error', err);
        });
    }
}
