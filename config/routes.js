module.exports = function (app) {
    app.get('/api/getCategories', require('../app/get-categories'));
    app.delete('/api/deleteLink', require('../app/delete-links'));
    app.post('/api/saveCategories', require('../app/save-categories'));
    app.post('/api/editCategories', require('../app/edit-categories'));
    app.post('/api/checkLinks', require('../app/check-links'));
    app.post('/api/saveLinks', require('../app/save-links'));
    app.post('/api/deleteCategories', require('../app/delete-categories'));
};
