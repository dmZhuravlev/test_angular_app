module.exports = function(req, res) {
    var request = require('superagent');
    var url = req.body.url;

    if (!url) {
        res.status(400).send('url is not correct');
    }

    request
    .get(url)
    .end(function (err, response) {
        if (err) {
            return res.status(400).send('url is not correct');
        }

        require('../helpers/checkUrl.js')(response, res);
    });
};
