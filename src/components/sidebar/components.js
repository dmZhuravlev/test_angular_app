import template from './sidebar-template.html';
import controller from './controllers';

export default () => ({
    template,
    controller,
    restrict: 'E',
    replace: true,
    controllerAs: 'sidebarCtrl',
    require: {},
    bindToController: true
});
