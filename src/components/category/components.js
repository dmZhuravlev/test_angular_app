import template from './category-template.html';
import controller from './controllers';

export default () => ({

    template,
    controller,
    restrict: 'E',
    replace: true,
    controllerAs: 'categoryCtrl',
    scope: {
        category: '=',
        categories: '='
    },
    bindToController: true

});
