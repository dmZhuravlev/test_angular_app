var mongoose = require('mongoose');
var Category = mongoose.model('Category');

module.exports = function (req, res) {
    var query = {
        _id: req.body._id
    };
    var update = {
        name: req.body.newName
    };
    var options = {
        new: true
    };

    Category.findOneAndUpdate(query, update, options, function (err, category) {
        if (err) {
            res.status(403).send(err);
        } else {
            res.status(200).send(category);
        }
    });
};
