var mongoose = require('mongoose');
var Category = mongoose.model('Category');

module.exports = function (req, res) {
    Category.update({_id: req.query.categoryId}, {
            $pull: {
                'links': {
                    _id : req.query.linkId
                }
            }
        }, function(err) {
            if (err) {
                return res.status(400).send(err);
            }
            Category.findOne({'_id': req.query.categoryId}, function (err, category) {
                if (err) {
                    return res.status(400).send(err);
                }
                res.status(200).send(category);
            });
        });
};