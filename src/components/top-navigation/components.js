import template from './top-navigation-template.html';
import controller from './controllers';

export default () => ({
  template,
  controller,
  restrict: 'E',
  replace: true,
  controllerAs: 'topNavigationCtrl',
  require: {

  },
  bindToController: true
});
