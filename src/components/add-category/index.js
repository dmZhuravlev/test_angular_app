import './addCategory.less';

import ng from 'angular';

import AddCategoryComponent from './components';

export default ng.module('app.components.addCategory', [])
    .directive('addCategory', AddCategoryComponent)
    .name;
