export default class LinkPanelController {
    constructor(CategoriesService, $scope, $stateParams, $state, $http) {
        'ngInject';
        this.$scope = $scope;
        this.$state = $state;
        this.$http = $http;
        this.$stateParams = $stateParams;
        this.getCategoriesService = CategoriesService;
    }

    $onInit() {
        this.getData();
    }

    getData() {
        this.getCategoriesService.getCategories().then((data) => {
            this.getCategories(data);
        });
    }

    getCategories(data) {
        let categories = data.data;
        let stateParamsId = this.$stateParams.id;

        if (!stateParamsId) {
            //this.$scope.activeCategoryLinks = categories[0].links;
            this.$state.go('details', {
                id: categories[0]._id
            });
        } else {
            categories.forEach((item) => {
                (item._id === stateParamsId) && (this.$scope.activeCategoryLinks = item.links)
            });
        }
    }
    renderLinks(category) {
        this.$scope.activeCategoryLinks = category.links;
    }
    deleteLink (id) {
        this.$http.delete('/api/deleteLink', {
            params: {
                linkId: id,
                categoryId: this.$stateParams.id
            }
        })
        .success(data => {
            this.renderLinks(data);
        })
        .error(err => {
            console.log(err);
        });
    }

}
