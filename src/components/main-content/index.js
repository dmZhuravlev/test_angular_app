import './mainContent.css';


import ng from 'angular';

import MainContentComponent from './components';
import AppCategories from '../commons/appCategories/index';

export default ng.module('app.components.mainContent', ['ui.bootstrap', AppCategories])
    .directive('appMainContent', MainContentComponent)
    .name;
