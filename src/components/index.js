import angular from 'angular';

import TopNavigation from './top-navigation';
import Sidebar from './sidebar/index';
import MainContent from './main-content/index';
import Category from './category/index';
import LinkPanel from './link-panel/index';
import AddCategory from './add-category/index';
import MainWrapComponent from './components';

export default angular.module('app.components', [TopNavigation, Sidebar, MainContent, Category,
LinkPanel, AddCategory]).directive('mainWrap', MainWrapComponent).name;
