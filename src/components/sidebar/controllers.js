export default class SidebarController {
    constructor($http, $compile, $scope, CategoriesService) {
        'ngInject';
        this.$http = $http;
        this.$compile = $compile;
        this.$scope = $scope;
        this.getCategoriesService = CategoriesService;
    }

    $onInit() {
        this.getData();

        this.showInput = false;
        this.showError = false;
        this.showButton = true;
    }

    getData() {
        this.getCategoriesService.getCategories().then((data) => {
            this.categories = data.data;
        });
    }

    appendInput() {
        const elem = document.createElement('div');

        elem.innerHTML = '<add-category categories="sidebarCtrl.categories"></add-category>';
        document.getElementById('container-for-inputs')
            .insertBefore(elem, document.getElementById('sidebar-btn-add'));
        this.$compile(elem)(this.$scope);
        this.showButton = false;
    }
}
