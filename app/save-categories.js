var mongoose = require('mongoose');
var Category = mongoose.model('Category');

module.exports = function (req, res) {
    var newCategory = new Category({
        name: req.body.name,
        isProtected: false,
        links: []
    });

    newCategory.save(function (err, category) {
        if (err) {
            res.status(403).send(err);
        } else {
            res.status(200).send(category);
        }
    });
};
