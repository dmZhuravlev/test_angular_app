var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var schema = new Schema({
    name: {
        type: String,
        unique: true,
        trim: true
    },
    isProtected: {
        type: Boolean
    },
    links: [{
        isImg: Boolean,
        url: {
            type: String,
            trim: true
        },
        comments: [],
        title:{
            type: String,
            trim: true
        },
        description:{
            type: String
        }
    }]
}, {
    collection: 'category'
});

var Category = mongoose.model('Category', schema);
module.exports = Category;
