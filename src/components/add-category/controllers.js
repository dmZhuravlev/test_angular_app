export default class AddCategoryController {
    constructor($http) {
        'ngInject';
        this.$http = $http;
    }

    $onInit() {
        this.showError = false;
        this.showErrorEmpty = false;
    }

    saveCategory(event) {
        if (event.keyCode === 13) {
            this.showError = false;
            this.showErrorEmpty = false;

            if (event.target.value === '') {
                this.showErrorEmpty = true;
            } else {
                const target = event.target.parentNode;
                const category = {
                    name: event.target.value
                };
                this.$http.post('/api/saveCategories', category)
                    .success(data => {
                        this.categories.push(data);
                        target.parentNode.removeChild(target);
                        this.sidebarCtrl.showButton = true;
                    })
                    .error(err => {
                        console.log(err);
                        this.showError = true;
                    });
            }
        }
    }
}
