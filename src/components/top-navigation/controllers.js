export default class TopNavigationController {
    constructor($scope) {
        'ngInject';

        this.$scope = $scope;
    }
    $onInit () {
        this.$scope.hideShowSideBarProp = false;
    }
    hideShowSideBar () {
        this.$scope.hideShowSideBarProp = !this.$scope.hideShowSideBarProp;
    }
}
