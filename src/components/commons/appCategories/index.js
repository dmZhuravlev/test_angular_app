import angular from 'angular';

import GetCategoriesService from './service';
import ngResource from 'angular-resource';

export default angular.module('app.app-categories', [ngResource])
    .service('CategoriesService', GetCategoriesService)
    .name;