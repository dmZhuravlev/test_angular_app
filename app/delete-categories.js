var mongoose = require('mongoose');
var Category = mongoose.model('Category');

module.exports = function (req, res) {
    Category.findByIdAndRemove(req.body._id, function (err, category) {
        if (err) {
            res.status(404).send(err);
        } else {
            res.status(200).send(category);
        }
    });
};
