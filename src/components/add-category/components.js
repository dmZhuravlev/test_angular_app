import template from './addCategory-template.html';
import controller from './controllers';

export default () => ({
    template,
    controller,
    restrict: 'E',
    replace: true,
    controllerAs: 'addCategoryCtrl',
    scope: {
        categories: '='
    },
    require: {
        sidebarCtrl: '^appSidebar'
    },
    bindToController: true
});
