export default class GetCategorysServise {
    constructor($q, $resource) {
        'ngInject';
        this.$q = $q;
        this.$resource = $resource('/api/getCategories');
    }
    getCategories() {
        if (this.promise) {
            return this.promise;
        }

        if (this.categories) {
            this.promise = this.$q.when(this.categories);
        } else {
            this.promise = this.$resource.get().$promise;

            this.promise.then((data) => {
                this.categories = data;
            });
        }

        return this.promise;
    }
}