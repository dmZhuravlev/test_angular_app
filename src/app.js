import angular from 'angular';

import 'bootstrap/dist/css/bootstrap.css';
import './style.css';
import 'angular-ui-bootstrap';
//import ngRoute from 'angular-route';
import uiRouter from 'angular-ui-router';

import Components from './components/index';

angular.module('app', [Components, 'ui.bootstrap', uiRouter])
    .config(function ($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.when('', '/categories');
        $urlRouterProvider.when('/', '/categories');

        $urlRouterProvider.otherwise('/');

        $stateProvider
            .state('categories', {
                url: '/categories',
                template: '<app-main-content></app-main-content>'
            })
            .state('home', {
                url: '/home',
                template: '<div>SOME</div>'
            })
            .state('details', {
                url: '/categories/:id',
                template: '<app-main-content></app-main-content>'
            });
        //$urlRouterProvider
        //    .when('/', {
        //        template: '<app-main-content></app-main-content>'
        //    })
        //    .when('/home', {
        //        template: '<div>SOME</div>'
        //    })
        //    .otherwise({
        //        redirectTo: '/'
        //    });
    });
