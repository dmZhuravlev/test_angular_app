import './category.css';

import ng from 'angular';

import CategoryComponent from './components';

export default ng.module('app.components.category', [])
    .directive('category', CategoryComponent)
    .name;
