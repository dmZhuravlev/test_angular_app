import template from './mainContent-template.html';
import controller from './controllers';

export default () => ({

    template,
    controller,
    restrict: 'E',
    replace: true,
    controllerAs: 'mainCtrl',
    bindToController: true

});
