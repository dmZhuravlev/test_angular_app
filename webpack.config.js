var webpack = require('webpack');
var CleanPlugin = require('clean-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var path = require('path');

module.exports = {
    context: path.join(__dirname, '/src'),
    entry: './app',
    output: {
        path: path.join(__dirname, '/build'),
        filename: 'bundle-[hash].js'
    },
    devtool: "source-map",
    watch: true,
    resolve: {
        root: [path.join(__dirname, '/src')]
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                loader: 'babel?presets=es2015',
                exclude: /node_modules/
            },
            {
                test: /\.html$/,
                loader: 'html'
            },
            {
                test: /\.css$/,
                loader: ExtractTextPlugin.extract('style', 'css')
            },
            {
                test: /\.less/,
                loader: ExtractTextPlugin.extract('style', 'css!less?sourceMap')
            },
            {
                test: /\.(woff2?|ttf|eot|svg)(.*)?$/,
                loader: "file?name=fonts/[name].[ext]"
            }
        ]
    },
    plugins: [
        new CleanPlugin(['build']),
        new ExtractTextPlugin('[name]-[hash].css'),
        new webpack.optimize.DedupePlugin(),
        new HtmlWebpackPlugin({
            template: path.join(__dirname, '/src/index.html'),
            inject: 'body'
        })
    ]
};