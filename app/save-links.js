module.exports = function(req, res) {
    var request = require('superagent');
    var url = req.body.url;
    var id = req.body.id;


    request
    .get(url)
    .end(function (err, response) {
        if (err) {
            return res.status(400).send('url is not correct');
        }

        require('../helpers/checkAndSaveLinks.js')(response, res, id);
    });

};