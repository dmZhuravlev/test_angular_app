import './sidebar.less';

import angular from 'angular';

import SidebarComponent from './components';
import AppCategories from '../commons/appCategories/index';

export default angular.module('app.components.sidebar', [AppCategories])
    .directive('appSidebar', SidebarComponent)
    .name;
