
import angular from 'angular';

import TopNavComponent from './components';

export default angular.module('app.components.top-nav', [])
    .directive('topNavigation', TopNavComponent)
    .name;
