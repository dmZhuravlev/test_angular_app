var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');

var app = express();
app.use(bodyParser.json());
app.use(express.static(__dirname + '/build'));
mongoose.connect('mongodb://zhur:access@ds053788.mongolab.com:53788/storage');

require('./model/category.js');

require('./config/routes.js')(app);

//app.listen({
//  port: 3001,
//  host: '10.10.54.28'
//});
app.listen(3001);

console.log("App listening on port ", 3001);
