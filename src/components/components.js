import controller from './controllers';

export default () => ({
    controller,
    controllerAs: 'mainWrapCtrl',
    scope: {
        hideShowSideBarProp: '='
    },
    bindToController: true
});
