import template from './modalDelete.html';

export default class CategoryController {
    constructor($http, $uibModal) {
        'ngInject';
        this.$http = $http;
        this.$uibModal = $uibModal;
    }

    $onInit() {
        this.showEditInput = false;
        this.showError = false;
        this.showErrorEmpty = false;
    }

    edit() {
        this.showEditInput = !this.showEditInput;
        this.showError = false;
        this.showErrorEmpty = false;
    }

    editCategory(event) {
        if (this.showEditInput === false || event.keyCode === 13) {
            this.showEditInput = false;
            const currentCategoryName = this.category.name;
            for (let i = 0; i < this.categories.length; i++) {
                if (this.categories[i].name === currentCategoryName) {
                    var currentCategory = this.categories[i];
                }
            }

            currentCategory.newName = event.target.value;
            if (currentCategory.newName === '') {
                this.showError = false;
                this.showErrorEmpty = true;
                this.showEditInput = true;
            } else {
                this.$http.post('/api/editCategories', currentCategory)
                    .success(data => {
                        for (let i = 0; i < this.categories.length; i++) {
                            if (this.categories[i].name === currentCategoryName) {
                                this.categories[i].name = data.name;
                            }
                        }
                        this.showErrorEmpty = false;
                        this.showError = false;
                    })
                    .error(err => {
                        this.showErrorEmpty = false;
                        this.showError = true;
                        this.showEditInput = true;
                    });
            }
        }
    }

    delete() {
        let currentCategoryName = this.category.name;
        for (let i = 0; i < this.categories.length; i++) {
            if (this.categories[i].name === currentCategoryName) {
                var currentCategory = this.categories[i];
            }
        }
        this.openModal(currentCategory);
    }

    openModal(currentCategory) {
        this.$uibModal.open({
            template,
            controller: deleteController,
            bindToController: true,
            controllerAs: 'deleteCtrl',
            resolve: {
                currentCategory: () => currentCategory,
                categories: () => this.categories
            }

        });
    }

}

class deleteController {
    constructor($uibModal, currentCategory, $http, categories) {
        'ngInject';
        this.$uibModal = $uibModal;
        this.currentCategory = currentCategory;
        this.$http = $http;
        this.categories = categories;
    }

    delete() {
        this.$http.post('/api/deleteCategories', this.currentCategory)
            .success(data => {
                for (let i = 0; i < this.categories.length; i++) {
                    if (this.categories[i].name === data.name) {
                        this.categories.splice(i, 1);
                    }
                }
                this.close();
            })
            .error(err => {
                // this.showError = true;
            });
    }

    close() {
        this.$close();
    }
}
