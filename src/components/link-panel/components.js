import template from './linkPanel-template.html';
import controller from './controllers';

export default () => ({

    template,
    controller,
    restrict: 'E',
    replace: true,
    controllerAs: 'linkPanelCtrl',
    bindToController: true

});
