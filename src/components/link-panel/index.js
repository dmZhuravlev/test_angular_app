import './linkPanel.css';

import ng from 'angular';

import LinkPanelComponent from './components';
import AppCategories from '../commons/appCategories/index';

export default ng.module('app.components.linkPanel', [AppCategories])
    .directive('linkPanel', LinkPanelComponent)
    .name;
